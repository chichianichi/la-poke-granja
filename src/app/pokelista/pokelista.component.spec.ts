import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokelistaComponent } from './pokelista.component';

describe('PokelistaComponent', () => {
  let component: PokelistaComponent;
  let fixture: ComponentFixture<PokelistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokelistaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokelistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
