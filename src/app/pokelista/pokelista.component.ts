import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-pokelista',
  templateUrl: './pokelista.component.html',
  styleUrls: ['./pokelista.component.css']
})
export class PokelistaComponent implements OnInit {
  pokemons: any[] = [];
  randomNumbers: any[] = [];

  constructor(
    private dataService: DataService
  ) { }

  ngOnInit(): void {    
    this.generarLista();       
  }

  /*Genera arreglo aleatoreo y hace get a httpClient*/
  generarLista(){
    for(let i=0; i<12; i++){
      this.randomNumbers[i] = Math.floor(Math.random() * (899 - 1) + 1);
      this.dataService.getPokemons(this.randomNumbers[i])
      .subscribe((response: any) => {
        this.pokemons[i] = response;
      });
    }
    console.log(this.randomNumbers);
    console.log(this.pokemons);
  }
}
