# La Pokegranja
<p>Proyecto de práctica para el desarrollo Frontend de páginas dinámicas con Angular.<br>
La característica principal es la comunicación HTTP con una RESTful API, así traer información y presentarla en pantalla.
 <br>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con Angular</li>
    <li>Uso de [HttpClient](https://angular.io/api/common/http/HttpClient)</li>
    <li>Peticiones a RESTful API ([PokéAPI](https://pokeapi.co))</li>
    <li>Vistas con HTML.</li>
    <li>Estilos con CSS</li>
    <li>Bootstrap</li>
    <li>Elementos dinámicos</li>           
</ul>

### Integración continua
<p>El proyecto está configurado para integración continua con GitLab CI/CD y cuenta con una pipeline de 4 pasos para la rama master:</p>
  <ul>
    <li>Install</li>
    <li>Test</li>
    <li>Build</li>
    <li>Deploy</li>
  </ul>


<h2>Ve el proyecto funcionando</h2>
<p>Ahora es posible visualizar la página en ejecución, previamente se ha configurado el deploy para poder ejecutar el website dinámico con la ayuda de la herramienta GitLab Pages, en conjunto con GitLab CI y GitLab Runner.</p>

### Watch online
##### Si te gustaría probar la página en ejecución has click  [Aquí](https://chichianichi.gitlab.io/la-poke-granja/  "Aquí")

